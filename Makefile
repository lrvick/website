.PHONY: default
default: \
	out/index.html

out/index.html: $(shell git ls-files theme static resume.yaml *.json)
	rm -rf out
	mkdir -p out
	cp -R static/* out/
	docker build --progress=plain -f Containerfile --output out .

serve: out/index.html
	env -C out python -m http.server
